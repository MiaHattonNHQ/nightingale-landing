---
title: {{ .Title }}
date: {{ .Lastmod }}
{{with .Params.author}}author: {{.}}{{end}}
---

{{ with .Params.description }}{{- .  | markdownify  -}}{{ else }}{{ .Summary }}{{ end }}

## Course overview
{{ .Params.value_proposition }}

### {{ .Params.feature_highlight_title }}
{{ .Params.feature_highlight }}

{{ $all_features := index .Site.Data.en "course-features" }}
{{ $selected := .Params.course_features}}
{{ range $all_features }}
{{ if in $selected .title }}
### {{ .title }}
![{{ .title }}]({{ .image | absURL }})

{{ .description }}
{{ end }}
{{ end }}
## Course outcomes
{{ .Params.outcomes }}

## Course fit
{{ .Params.prerequisites }}

### Recommended groups
{{ $all_features := index .Site.Data.en "audiences" }}
{{ $selected := .Params.audiences}}
{{ range $all_features }}
{{ if in $selected .title }}
### {{ .title }}
![{{ .title }}]({{ .image | absURL }})

{{ .description }}
{{ end }}
{{ end }}
## Industry led
{{ $selected := .Params.experts}}
Our training is delivered by a wide range of world-class practitioners who have a strong track record successful AI and data training and adoption across many industries.

{{ range where $.Site.RegularPages "Section" "experts" }}
### {{ .Title }}
![{{ .Title }}]({{ .Params.image | absURL }})

{{ .RawContent }}
{{ end }}
## Course structure
{{ .Params.course_structure  }}

## Course details
{{ .Params.course_details }}

## Time commitments
{{ .Params.time_commitment_text  }}

## Course deliverables
{{ .Params.deliverables }}

## Course outputs
{{ $all_features := index .Site.Data.en "credentials" }}
{{ $selected := .Params.credentials}}
{{ range $all_features }}
{{ if in $selected .title }}
### {{ .title }}
![{{ .title }}]({{ .image | absURL }})

{{ .description }}
{{ end }}
{{ end }}

## Additional information
{{ .RawContent  }}