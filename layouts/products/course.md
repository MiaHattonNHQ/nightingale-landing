---
title: {{ .Title }}
date: {{ .Lastmod }}
---

{{ with .Params.description }}{{- . | trim "  " | markdownify  -}}{{ else }}{{ .Summary }}{{ end }}

Course