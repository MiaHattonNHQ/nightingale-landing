---
title: {{ .Title }}
date: {{ .Lastmod }}
{{with .Params.author}}author: {{.}}{{end}}
---

{{ with .Params.description }}{{- . | trim "  " | markdownify  -}}{{ else }}{{ .Summary }}{{ end }}
