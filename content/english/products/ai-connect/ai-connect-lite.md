---
title: "AI Connect Lite"
date: 2020-03-02T12:52:36+06:00
image_webp: images/about/screenshot.webp
image: images/about/screenshot.jpg
author: Ruth Kearney
aliases:
  - how-ai-connect-works
  - how-it-works
description : "Accelerate your AI projects"
availability: "OnlineOnly"
menu:
  products:
    weight: 3
---

Rapidly shortlist expert data & AI consultants to help you deliver your projects. Connect to them directly and get started.

To get started, simply provide information about your company and the sort of project you're looking to intitiate. Our ranking algorithm will then sort our growing panel of expert consultants to provide you with  a shortlist. Reach out to your suggested partners and get your projects rolling.

## Find a consultant
In the coming year, 90% of business leaders are planning to implement some form of AI in their organisation, however 65% of executives who have already done so are reporting failure. Conversely, those who are getting Artificial Intelligence and Machine Learning right, plan to increase use companywide and reap the profits.
  
So how do you keep up with crowd and make sure you get it right?
  
That’s where we come in.

## A sophisticated match
With the right guidance, your company could excel in their data science adventures. That’s why our algorithm is designed to match you with an expert based on your project criteria and their expertise.

It’s like Google but for finding your expert.

Just fill in your profile so that our algorithm can get to work and we’ll show you your matches, saving you time, money and effort.

## What happens next?
Once you’ve found your match, we leave you to negotiate the details of your project with your new data or AI partner. But of course, you can come back whenever you like! We're always rolling out new capabilites to improve your AI journey. To have a say in the development of future products, you can also join our <a href="https://landing.nightingalehq.ai/join-our-research-panel"> research panel</a>
  
  > Sign up for AI Connect Lite and get connected with an expert data & AI consultant. [Register now](https://app.nightingalehq.ai)