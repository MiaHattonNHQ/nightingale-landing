---
title: DataHack Bootcamp for Technical Support Teams
id: DHBC112
type: course
category: business
course_type: Bootcamp
date: 2020-04-28T22:00:44.962Z
author: Team
tags:
  - AI
  - help for organisations
categories:
  - Offerings
image: images/blog/blog-post-1.jpg
description: This bootcamp is for technical support teams who need to learn
  about how Artificial Intelligence (AI) is transforming business.
audiences:
  - Technical Support Team
  - Customer Service Teams
  - Customer Success Team
offers:
  - Teams
  - Enterprise
  - Associations
course_features:
  - Action projects
  - Live lectures
  - On-demand
  - Industry-led
experts:
  - Steph Locke
testimonial_1: "Business impact of AI "
testimonial_2: "Accelerated my career "
credentials:
  - For enterprise
  - For participants
  - Training participation certificate
outcomes: >-
  On completion of this course participants will:


  * Understand what AI is how it impacts business 

  * Learn about automating manual tasks

  * Recognise AI tools/processes for growth & revenue opportunities

  * Recognise the value of Data Visualisation as an effective sales/retention tool

  * Apply Data Visualisation skills to communicate value more effectively

  * Apply Data Journalism and Social Listening methods to work

  * Understand Responsible AI (Trust, Bias, Ethics)

  * Learn about the basics of data compliance and GDPR.
value_proposition: >-
  This DataHack Bootcamp is designed to increase the overall level of data
  literacy of your organisation and brings your teams up to speed with how AI is
  transforming business. Over the course of seven weeks, learners take a
  deep-dive into how data science applies to customers service and how key
  topics including Artificial Intelligence, Data Visualisation and Data
  Analytics can drive better growth & revenue opportunities for business.


  The programme has been designed for any organisation who need an understanding of how this disruptive technology is changing the way companies market their products and services. It develops teams who are making data-driven decisions for clients every day and seek to improve how to analyse, diagnose and resolve issues more effectively.


  It makes good business sense to future-proof important skillsets as the market for AI products and services is predicted to grow exponentially. We have created a course that develops AI-readiness across your organisation in a very real and practical way. 
feature_highlight_title: On-demand access
feature_highlight: Never miss out on lessons as all our content is recorded and
  participants will receive full access. Self-paced learning, whenever and with
  complete flexibility. In conjunction with live classes our recordings
  reinforce learning.
course_structure: >-
  The Bootcamp consists of activities occurring over 7 week time frame. We
  combine multiple methods of teaching to reinforce learning including live
  lectures, labs, mentoring, assignments and action learning project
  challenges. 


  ### Course Overview


  | Timeframe         | Topics                                                        | Components                                              |

  | --------------------- | ----------------------------------------------------------------- | ------------------------------------------------------------ |

  | 3-4 week before start | Pre-course work                                                   | Project scope complete. Reading & Resource list released     |

  | Week 1                | Introduction to AI. How does it add value to business?            | AI Quick Win Activity Use cases and application Assignment 1 |

  | Week 2                | Data Infrastructure Overview and the process of Customer Service  | Assignment 2                                                 |

  | Week 3                | AI for cost saving and automation; tools and process improvements | BYOD Project Launch                                          |

  | Week 4                | AI tools/processes for growth & revenue opportunities             |                                                              |

  | Week 5                | Data Visualisation as an intelligence tool                        |                                                              |

  | Week 6                |                                                                   |                                                              |

  | Week 7                | AI and my role                                                    | BYOD Pitches                                                 |


  <!--more-->


  ### Reading/Resource list


  A core resource/reading list will be shared in advance of the course, ensuring that the main focus of the classes will be on contextualisation, critical discussion around the relationship between theory and practice.


  ### Action learning


  A large part of this course is about applying theoretical concepts and skills to real data challenges facing an organisation. Project work termed Bring Your Own Data (BYOD) is a vital part of realising this. It is designed with the aim of delivering actionable solutions and recommendations for practical implementation. It is also an opportunity to learn and be mentored by experienced practitioners who will support the practical implementation of the project work.


  ### Course evaluation


  The quality of the course is maintained via course evaluation surveys and follow up interviews. We value all feedback and continuously look to enhance our content, delivery, application and outcomes. Participants of the course are required to fill out both pre-course and post-course surveys. Trainers are also required to fill out evaluation surveys.
deliverables: >-
  The bootcamp is outcome-oriented with practical deliverables developed at
  different stages. Participants will have the following tangible outputs on
  completion. In terms of feedback on deliverables, there is a mixture of
  formative and summative that focuses on knowledge, reflection and application
  of skills and competencies. 


  | Course component | Deliverables | Feedback/Evaluation |

  |-------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------|

  | 7 weeks online sessions | Attendance recommended | Online sign-in |

  | 3 assignments | Short practical activities to implement tools/methods to participants own work. | Online feedback with trainer |

  | BYOD Project | Final pitch presentation (12 minutes)   Final written report (3500 – 5000 words)    Data output such as organisation process improvement or custom visualisation | Written feedback  Online feedback sessions with trainer |

  | Reflection assignment | Short written piece where  participants reflect on their learning experience and how they have applied learning within their own context. (1000 words) Opetional. | Self-evaluation. |

time_commitment:
  quantity: 35
  period: H
time_commitment_text: >-
  This course has an estimated 40 hours commitment over a 7-week timeframe. This
  consists of 7 weeks of live online classes (2 hours per week) totally 14
  hours. In addition, there is 3 hours of mentoring support. Non-contact hours
  (outside of live sessions and mentoring) include pre-course preparation,
  reading time and project work is estimated to be a minimum of 23 hours.


  <!--more-->


  ### Summary


  | Component   | Hours | Activities                                    |

  | --------------- | --------- | ------------------------------------------------- |

  | Online classes  | 14        | Lecture, workshops, labs                          |

  | Pre-course work | 5         | Recommended Reading/recourse list   Project scope |

  | 1 BYOD project  | 12        | Bring Your Own Data project work.                 |

  | 2 Assignments   | 6         | Continuous assessment relating to key topics      |

  | Mentoring       | 3         | Individual and group online support               |

  | Total hours     | 40        |                                                   |


  ### Project work


  It is recognised that the ‘practice’ element of the bootcamp involves participants implementing learning within their own organisations. A communication platform such as Slack/Github or your own company platform will support collaborative learning (outside the formal learning). Project work entails outside formal class time work hours, which is self-directed group work.
lastmod: 2018-02-17T00:00:00.000Z
course-features:
  - 1-1 mentoring
  - Practical labs
  - Live lectures
  - On-demand
testimonials:
  - "Business impact of AI "
  - The real value of AI
prerequisites: >-
  This course is perfect for any team that is looking to make data-driven decisions, particularly in a client-facing role. The course will help people improve their ability to analyse, diagnose, and resolve issues more effectively.
course_details: >-
  | Date     | Rolling dates throughout the year depending on business
  requirments                                                                                                                    |

  | -------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |

  | Format   | Recommended format is 7 week Bootcamp with weekly classes for 90 minutes                                                   We can accommodate alternative times with sufficient notice |

  | Location | Online                                                                                                                                                                                 |

  | Cost     | This training is for groups of between 5 and 25 and is based on a per person basis  Please contact us to discuss your training requirements.                                           |

  | Trainer  | Steph Locke                                                                                                                                                                            |


  <!--more-->


  ### Hardware and software


  Participants are expected to successfully participate in lectures, laboratories and projects using their own portable computer (laptop/notebook). Standardised virtual machines to support learning will be provided. For participants who want to work from their own device especially if their company has data security policies will receive a list of items to install locally.


  ### Course updates


  The course has its own webpage containing all slides, assignments and announcements relating to the programme. All students are expected to register on this page for updates. Collaborative tools such as Slack, Microsoft Teams or your own company communications platform will be used for informal discussions and collaboration.
---
### Why take a bootcamp?

The bootcamp structure by definition is a relatively short-term, intense duration involving a robust balance of theory and practice, practitioner-led workshops, andmentorship with experienced practitioners. It gets you to where you want to go and quickly.

### Is attendance mandatory?

Attendance to the live sessions is highly recommended. 

### How are deliverables evaluated?

Both formative and summative feedback is provided both throughout the course and on completion of the course deliverables. The focuses is on knowledge and application of skills and competencies. Feedback takes the form written, individual and group sessions. 

### How is the course evaluated?

Both participants and trainers evaluate the course through feedback questionnaires and follow up 1-1 discussions. 

### What is the situation with hardware and software?

Virtual Machines with setup for the duration of the programme. For those who want to use their own device, a list of items to install will be shared.