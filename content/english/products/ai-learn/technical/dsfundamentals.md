---
title : "Data Science Fundamentals"
lastmod : "2018-02-17"
draft: true
---

This course will take you from no data science knowledge to understanding the fundamentals and being able to code your own solutions.

## Data Science Fundamentals

In this two-day course, we’ll take you from no data science knowledge to understanding the fundamentals and being able to code your own solutions. Using R, we'll cover everything from the process of doing data science to the details around sampling and model selection.

## Learning Outcomes

- Overview of Data Science.
- Business Understanding Determining the right question to ask and understanding the processes that you'll be impacting.
- Data Understanding Gathering data and putting it into the right format for later work.
- Data preparation Cleaning up data, joining additional  datasets, performing feature engineering to improve your ability to generate great data science outputs.
- Modeling Sampling your data and building multiple models to answer your data science question.
- Evaluation Working out which is the best model to meet your needs.
- Deployment Options for getting a model live and things you need to take into consideration.

## What our training days look like

We pair up teaching you important theory with practical exercises to get you confident in your use of the conceptual frameworks and tools you'll be learning. You'll work with real datasets so you'll always be able to see how you can apply things in real life.

## What you'll need on the day

You'll need a device with internet and RDP allowed and ideally a camera. You'll be doing exercises so that you get familiar with the code and the concepts, as well as viewing the online course handouts, and accessing the training group's Slack channel. We use online coding environments so all you need to have installed is a web browser (for best results this should be a recent version of Chrome or Firefox).

## What you'll receive

Before the event we'll invite you into our Slack workspace and the private channel for your course. We'll share links, useful pre-reading, and more. This'll be a place you can ask questions during the course and post achievements afterwards.

On the day of the course, you'll be given access to our online materials and any lab environments, meaning you can spend more time learning and less time solving computer woes.

After the training, you'll have access to our online materials FOREVER. We'll also let you share the link inside your organisation. You'll also be able to access the lab environments for a week after the training so you can recap when you get back to work.
