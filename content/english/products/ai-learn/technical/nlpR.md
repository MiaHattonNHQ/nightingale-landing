---
title : "NLP in R"
lastmod : "2018-02-17"
draft: true
---

Attend this course to get hands-on with doing text analysis in R. We assume you know a bit of R already but we'll taking you through the principles and techniques for doing text analysis in R, as well as giving everyone copies of the brilliant Text Mining with R book for continued study.

## Overview of NLP in R

- Getting data from different sources
- Wrangling data into a useful format
- Analysing sentiment
- Identifying document topics
- Building a concept of relevancy
- Visualising text analysis outputs.

**Need to convince a manager? Give them our [Overview sheet](../../files/modern-analysts-toolkit-overview.pdf) for the training!**

## What our training days look like

We pair up teaching you important theory with practical exercises to get you confident in your use of the conceptual frameworks and tools you'll be learning. You'll work with real datasets so you'll always be able to see how you can apply things in real life.

## What you'll need on the day

You'll need a device with internet and RDP allowed and ideally a camera. You'll be doing exercises so that you get familiar with the code and the concepts, as well as viewing the online course handouts, and accessing the training group's Slack channel. We use online coding environments so all you need to have installed is a web browser and the ability to RDP.

## What you'll receive

Before the event we'll invite you into the Locke Data Slack workspace and the private channel for your course. We'll share links, useful pre-reading, and more. This'll be a place you can ask questions during the course and post achievements afterwards.

On the day of the course, you'll be given access to our online materials and any lab environments, meaning you can spend more time learning and less time solving computer woes.

After the training, you'll have access to our online materials FOREVER. We'll also let you share the link inside your organisation. You'll also be able to access the lab environments for a week after the training so you can recap when you get back to work
