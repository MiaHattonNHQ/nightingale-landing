---
title: NatWest Accelerator
date: 2019-03-30T01:00:00.000Z
description: Nightingale HQ joins the NatWest Entrepreneur Accelerator
tags:
  - Awards
categories:
  - Awards
image: images/download-1.jpg
author: Ruth Kearney
---
**Nightingale HQ joins the NatWest Entrepreneur Accelerator**

Nightingale HQ started on the NatWest Accelerator this month as part of a continued programme of readiness, product development, and networking for Nightingale HQ.

Gordon Merrylees, Managing Director Entrepreneurship, NatWest said: We are delighted to welcome Steph, Sarah, and Nightingale HQ on to the programme in Cardiff.


With our unique offering that provides the right environment, coaching, and networks, we help entrepreneurs start, scale and succeed every day. We look forward to seeing Nightingale HQ engage with the programme and grow their business with us.
With hubs distributed across the UK, NatWest are helping build local economies and drive change away from the London-centric perspective. 

I chose to stay in Cardiff and have been dedicated to growing the technical community here for nearly a decade. The talent drain to London has been something I’ve tried to actively fight and I’m proud we have such a flourishing tech scene and now a growing startup scene. Another great aspect of their program is they are working to support female entrepreneurs and 47% of their cohorts in 2018 identified as female. As an all-female founder team, we’re aware we face some additional challenges to many startups so we really value the active support both NatWest and Welsh Government are giving us as we build Nightingale HQ.

- - -

[Press kit](../../press)