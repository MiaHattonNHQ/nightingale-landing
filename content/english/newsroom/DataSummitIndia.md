---
title: Data Summit India
date: 2020-03-30
image: 
tags:
  - Awards
categories:
  - Awards
author: Ruth Kearney
---

**Technation.io**


**Company **
Whilst in India for DPS10, I got to discuss and explore my thoughts with the nifty Leila Etaati and Andy Leonard. They helped me understand that what I needed to do was something that would enable Locke Data and others to surface their knowledge and services around data science readiness to organisations around the world.

Conference https://www.dps10.com/
---
[Press kit](../../press)