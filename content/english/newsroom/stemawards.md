---
title: Stem Awards Wales 2020
date: 2020-03-30
image: images/newsroom/cardiff-life-awards.png 
tags:
  - Awards
categories:
  - Awards
author: Ruth Kearney
---

**Stem Awards Wales 2020**

**Describe your business**

Nightingale HQ is a complete AI platform that helps businesses get AI-Ready by providing content, training, and ultimately, connections. We offer three products, AI Learn, AI Direct and AI Connect. Through AI learn, companies can get training to get their teams AI ready. Through AI direct, we can help businesses asses their potential and build an AI strategy. Finally AI connect helps connect organisations with the worlds best AI experts, taking the pain out of finding an AI specialist and providing one simple point of contact to manage all projects.

Our online platform was originally set up with the expert matching system as a priority. This software is powered by an algorithm which identifies the best consultants on our site based on the needs of our clients. We are now building additional functionality for delivering training both online and offline and to support the project management aspect of our product.

**Why should your business win STEM Start-Up Of The Year**

Nightingale HQ was born in December 2018 receiving a starting push from founders Steph Locke and Sarah Williams to transform the idea from their heads into a reality.

2019 was an important year for the businesses, entailing lots of preparations and plenty of support from the likes of Welsh Accelerated Growth Programme and it’s Startup Accelerator, the NatWest Accelerator Hub, Welsh ICE’s ICE 50 Programme and HubSpot’s start up programme. These programs provided us with training days, co-working spaces for our staff, access to mentorships and a network of peers with skills to share, support from the Welsh Government, and a fabulous CRM system from HubSpot at 10% of the cost.

By June 2019 the website live and the platform received lots of sign ups from consultancies ready to help businesses with their AI needs, and by the end of 2019 we had 64 consultancies registered. By September we welcomed some new employees including two software engineering interns for a placement period, one of which we went on to hire, a Data Science Apprentice, made possible through the Welsh Government who supply the funding for the degree, and a Graduate Digital Marketer using the Cardiff Capital Region Graduate Scheme.

With the turn of 2020 Nightingale HQ went under further changes after receiving lots of support for their idea including interest from investors, but less action. We recruited an additional senior manager to focus on sales and made an adjustment to our offerings, with a greater focus on training and commitment to getting businesses AI-Ready.

Nightingale HQ as a company has demonstrated adaptability and drive as we enter our second year of business with a more robust plan to launch forward. So far Steph Locke has already won two awards in recognition for her efforts with Nightingale HQ. She was granted the Most Innovative Woman in Artificial Intelligence in the UK – 2019 at the Influential Businesswoman Awards, as well as CEO of 2019 at the Welsh SME Business Awards. Co-founder Sarah Williams also won the WalesOnline 35 Under 35 award.

Not only was Nightingale HQ is founded by outstanding women in tech who have both been recognised for such, but it is set out to help other businesses strive for greatness with AI initiatives, making it an excellent candidate for the STEM Start-up of the Year award.

**Why should you win STEM leader of the Year**

Steph Locke has been an inspiration and a starter when it comes to all things data and AI. Her passion for helping others seize opportunities in the digital age is evident through the numerous talks, workshops, meet ups and courses that she provides. She has even written two data science books to share her knowledge and contributed to others, set up sites and forums to foster and serve the tech community.

She is a Microsoft Most Valued Professional in both Data Platform and Artificial Intelligence, a combination awarded by Microsoft to only two other people in the world. She regularly presents and keynotes globally on the topics of AI, Data Science, and the integration of these capabilities into business.

Steph successfully created a consulting business to address the needs of organisations wanting to do data science, but realised that in order to help more people, she would need to scale, and rather than scale her consultancy, she would achieve this instead by creating a platform.

Now the CEO of two businesses, and recognised by the Welsh SME Business Awards as CEO of the year 2019, Steph continues to support the technical community and has created a conference, Data Opticon, which first ran in September 2019 and will run a second, US edition in March 2020.

Furthermore she has created inclusive opportunities within her business that allow people of all background to apply. The data science apprentice position that was filled in September allowed for the successful candidate to obtain a Data Science degree for free, thanks to support from the Welsh government. All roles within Nightingale HQ offer flexible working, another inclusive option that has allowed certain people.

Steph’s innovative thinking has led her companies forward in the right directions, and for this I think she makes an excellent candidate for the STEM leader of the tear award.

**How do you or your business further the STEM sector is Wales?**

As mentioned in previous categories, Nightingale HQ’s mission is to prepare businesses so that they are AI ready. We understand that taking on AI is a huge challenge and a risk, so by making the transition easier, we increase the chances of companies succeeding with AI and continuing to thrive in a world that is increasingly ruled by technology.

Our founder Steph Locke is an active member of the tech community and runs free, accessible events and meet ups to promote coding and support the community, including SatRdays and CaRdiff, R user events. The moment she catches wind of an AI or data focused event in the area, she’ll do everything she can to make sure she is there, delivering a talk to share her knowledge, sacrificing free time without a second thought.

First published online https://www.stemawards.wales/  
---
[Press kit](../../press)