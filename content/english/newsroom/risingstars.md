---
title: Rising Stars
date: 2020-03-30
image: images/newsroom/cardiff-life-awards.png 
tags:
  - Awards
categories:
  - Awards
author: Ruth Kearney
---

**Technation.io**


**Company **
Nightingale HQ helps SMEs identify cases for artificial intelligence within their business and provides the tools and support required to help them to successfully adopt and deploy AI and specific AI projects.

**Describe your business in three sentences**
Nightingale HQ is an online platform dedicated to helping SMEs adopt AI successfully. Customers can find the perfect data & AI consultancy to support their needs, using our personalised search. In the next few months, customers will be able to consume education materials and develop roadmaps using our expert knowledge.

**In more detail, tell us what your company does**
This is your opportunity to tell us in detail about your company. What is your vision? What is your competitive advantage/USP? Tell us about your journey so far - how did you get to where you are?

Most businesses want to grow faster, build new customer products, and profit. These businesses are often paying to store and manage data that could be more effectively used. Moving from wasting data to applying data-driven capabilities like Artificial Intelligence into business functions is a huge journey.

Many businesses report key gaps in executive understanding, internal skills, and access to their data as substantial blockers to successfully leveraging AI and accessing the estimated value it could bring.

AI products like chatbot solutions and augmented reality capabilities are already widespread. The problem isn't technology, it’s people. Businesses face cultural changes, companywide education, and interaction with experts in order to become data driven and leverage AI.

Our single integrated online platform will support this business journey, allowing SMEs to grow, innovate, and profit using a technology previously reserved for the big companies. Currently, SMEs can connect with global data & AI consultants across verticals, technology stacks, and specialisms, who can actualise AI strategy for businesses.

Future functionality includes an AI readiness assessment and planning tool, executive AI strategy training and prototype AI solutions.

We launched earlier this year our data & AI consultancy marketplace to enable leaders to connect with consultants who can help them further AI projects. Using a matching alogorithm, we refine their options based on location, vertical, technology stack, and current project requirements.

Over the next year, we're implementing a community learning space, an AI readiness assessment tool, and an implementation toolset.

**What is the name of your product / service?**
Nightingale HQ

**What product milestones have you achieved so far?**
Please list the most significant milestones you have achieved so far, starting each one in a new sentence/paragraph

**We launched the data & AI marketplace late June**
We've taken on a junior developer, a graduate digital marketer, and a data science apprentice to support the development of our growth
We've delivered increased functionality around the matching algorithm, service offerings from consultants, and compliance

**What market traction do you have?**
Who are your customers? Who are you selling to? Who are you partnering with?

More than 50 consultancies signed up
5 customers
50% growth on visitors from September to October

**What were your biggest achievements of the last 12 months?**
Please list your biggest achievements of the last 12 months, starting each one in a new sentence / paragraph. These could include clients, sales, revenue, media coverage etc.

5 pieces of press coverage for Nightingale
Achieved 1000% increase in organic impressions from September to October
Released the marketplace

**What are your key milestones for the next 12 months?**
Please list your key milestones for the next 12 months, starting each one in a new sentence / paragraph.

Secure funding
Get revenue generating
Deploy AI assessment
Deploy training courses
**What experience does your team have that proves you are best placed to succeed?**
Please do not feel too shy to sell yourself! This is your opportunity to talk about any experience you might have - whether this is your first business or you're a serial founder - academic credentials, fun facts, or anything else that helps us understand 'what makes you, you.'

Having both held senior technical positions in several companies, a fair share of start ups included, Steph and Sarah have both grown teams and assisted in aligning various technology functions to business goals. They share a passion for helping people seize opportunities in the digital age.

Steph is a Microsoft Most Valued Professional in both Data Platforms and Artificial Intelligence, a combination awarded by Microsoft to only two other people in the world. She regularly presents and keynotes globally on the topics of AI, Data Science, and the integration of these capabilities into business.

Sarah is a dedicated senior technologist with refined mentoring and presenting skills. She has been recognised for her work in startups with awards like Wales 35 under 35.

Article first published online https://technation.io/programmes/rising-stars/ 
---
[Press kit](../../press)