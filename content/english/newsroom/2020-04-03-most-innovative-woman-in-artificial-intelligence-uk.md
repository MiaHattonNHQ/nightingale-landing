---
title: Most Innovative Woman in Artificial Intelligence - UK
date: 2019-12-01T13:55:20.207Z
description: Steph Lock voted Most Innovative Woman in Artificial Intelligence - UK
tags:
  - news
categories:
  - news
image: images/new-ai-logo-2019-01b.png
---
**Acquisition International is Proud to Announce the Winners of the 2019 Influential Businesswoman Awards**

**United Kingdom, 2019**– Acquisition International Magazine have announced the winners of the 2019 Influential Businesswoman Awards.

![Influential Businesswoman Awards](images/stephlocke.png "Nightingale HQ – Stephanie Locke Most Innovative Woman in Artificial Intelligence - UK")

The Influential Businesswoman Awards returns once more to add their voice to the masses who believe gender equality is the only solution for a sustainable global economy. Promoting and encouraging women to take part in entrepreneurial endeavours and providing opportunities in market-based enterprises can be pivotal to lifting communities across the world.

Bearing this in mind, we recognise that female entrepreneurs still face many challenges. These include limited access to financing, training and business networks, in addition to the uncertain job markets and fluctuating economies shared with their male counterparts.

Considering these additional burdens, it seems imperative to recognise the achievements made by businesswomen globally; not just the 105 million self-employed women in the UK, or the 9.4 million woman-owned firms in the USA, but to all the businesswomen who have surmounted so many obstacles to attain professional success.

Discussing the programme in more detail, Jessie Wilson, Awards Coordinator, said: “All of my winners have broken the glass ceiling and helped herald a new era for women in business. As such, I am incredibly proud of their success and wish them the best of luck going forward.”

Acquisition International prides itself on the validity of its awards and winners. The awards are given solely on merit and are awarded to commend those most deserving for their ingenuity and hard work, distinguishing them from their competitors and proving them worthy of recognition.

To learn more about our award winners and to gain insight into the working practices of the “best of the best”, please visit the Acquisition International website ([http://www.acq-intl.com](https://www.acq-intl.com/)) where you can access the winners supplement.

First published online [here](https://www.acq-intl.com/the-2019-influential-businesswoman-awards-press-release/)