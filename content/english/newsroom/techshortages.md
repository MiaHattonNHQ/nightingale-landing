---
title: Tackling Tech Shortages
date: 2019-08-09T12:41:44.952Z
description: >-
  ‘FutureFutureproof your skillset with Machine Learning, R, Data Visualisation
  and PowerBI.’
tags:
  - data training
categories:
  - newroom
image: images/download.jpg
---
Tackling tech shortages - Talent Garden launches bootcamps in Data Science with industry faculty. 

As [Ireland](https://irishtechnews.ie/category/ireland) becomes the only English-speaking country in the European Union, demand for tech skills and talent is expected to rise. With demand already outstripping the supply of skilled people in the area of Data Science,[Talent Garden Dublin](https://dublin.talentgarden.org/) has launched a new practical, industry-led Bootcamp. The expert faculty that are leading the Bootcamp will help businesses and individuals up-skill and stay ahead of the game when it comes to sectoral trends and innovations needed to run successful organisations.

## Data Science Bootcamp

*‘Futureproof your skillset with Machine Learning, R, Data Visualisation and PowerBI.’*

Relevant to all business sectors including banking, retail, insurance, marketing and healthcare, data is ‘the new oil’. This 18-week part-time[Data Science Bootcamp](https://bit.ly/2YOhZMp) helps participants to interpret insights from data quicker and create greater impact in business through data mining, data visualisation, machine learning, AI and work practices. Participants will learn design and workflow patterns that take you from raw data to the next level and will also have the opportunity to work on an Industry Project to put their new skills into practice, partnering with real-world companies such as Epic Museum, Met Eireann, Anadue and Exit-Entry.

Industry experts leading the Bootcamp include Dr. Finn Macleod, Founder of Beautiful Data and DistilX.com, who has designed dashboards for clients such as Thomson-Reuters, Formula 1 and Heineken and Stephanie Locke, a Data Scientist, Entrepreneur and CEO of Nightingale HQ – an online platform to help businesses develop and implement their own data and AI strategies successfully.

This article was first published [here](https://irishtechnews.ie/talent-garden-bootcamp-data-science/?__cf_chl_jschl_tk__=39dd668367b2b4e6e3fb083f6a77eea88887b8ed-1585917315-0-AQczI7WA47um7HwIHl1fjhekGmL59LTXTDiGhDaOQaRVa-LmcbVP76Zx2sqtjSFb-zI0-MuuZtqcZl04MQt6U2X2AayTacHLwbCcmHUaNQOK1jLBEpzC_c9dzwYpkGRIpZ-G5mZhRXN68rjzPfWEL2eA7QLvBc7Yx23PFjiUqa3KhoKn4H0A8US07HYoM2U-dRwfhJbEpbgOvugPwEwjPeXDD52ZrQo0J_QhT1f4rGe_wcV9kpH6vZfbOiWEP2fmSvitKyTgTCtnrEDKSTk-TMzKx27bnQ50z_DHIaZLL4-9NysYg1psWS8n_7SWW69BrA)