---
title: Data warehouse
image: /images/uploads/database-maintenance.jpg
description: Data warehousing refers to building a system that can store large volumes of data from several sources, as a central repository from which all other systems can access the data efficiently.
---

Data warehousing refers to building a system that can store large volumes of data from several sources, as a central repository from which all other systems can access the data efficiently.
