---
title: Azure
image: /images/uploads/azure.jpg
description: Microsoft Azure is a set of cloud services that help your organisation meet your business challenges.
---

Microsoft Azure is a set of cloud services that help your organisation meet your business challenges.
