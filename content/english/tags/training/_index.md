---
title: Training
image: /images/uploads/training.jpg
description: While it can be beneficial for a company to outsource data science skills, we recommend building up data skills within your team by upskilling and recruiting where possible as part of your data science strategy.
---

While it can be beneficial for a company to outsource data science skills, we recommend building up data skills within your team by upskilling and recruiting where possible as part of your data science strategy.
