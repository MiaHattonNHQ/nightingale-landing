---
title: AWS (Amazon Web Services)
image: /images/uploads/aws.png
description: AWS is a cloud computing platform, which allows pay-as-you-go access to storage and computing services without expensive infrastructure set-up fees.
---

AWS is a cloud computing platform, which allows pay-as-you-go access to storage and computing services without expensive infrastructure set-up fees.
