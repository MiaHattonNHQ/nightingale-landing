---
title: Strategy
image: /images/uploads/data-strategy.jpg
description: Data science strategy refers to a company's vision for how data will be used to help achieve the company's business goals, how to build a thriving data culture, and how to address the skills and knowledge required to execute this vision.
---

Data science strategy refers to a company's vision for how data will be used to help achieve the company's business goals, how to build a thriving data culture, and how to address the skills and knowledge required to execute this vision.
