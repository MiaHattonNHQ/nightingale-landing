---
title: Tags
image: /images/uploads/tag.jpg
description: Tags are how we build up a profile of organisations and partners to support the process of matching them up.
---
Tags are how we build up a profile of organisations and partners to support the process of matching them up.

We have tags in different sections but we also have a custom Tag section. In this area, you can add anything that isn't available or doesn't quite fit into profile tag sections.