---
title: business intelligence
image: /images/uploads/business-intelligence.jpg
description: Business intelligence refers to the systematic collection, integration, analysis and reporting of data from all areas of your business to drive better decision making.
---

Business intelligence refers to the systematic collection, integration, analysis and reporting of data from all areas of your business to drive better decision making.
