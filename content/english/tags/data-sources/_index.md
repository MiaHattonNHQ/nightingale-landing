---
title: Data sources
image: /images/uploads/data-sources.jpg
description: Your data can come from a variety of different sources, but many technologies are available to integrate the sources and gain insight from the data.
---

Your data can come from a variety of different sources, but many technologies are available to integrate the sources and gain insight from the data.
