---
title: Data culture
image: /images/uploads/data-culture.jpg
description: Curating a healthy data culture is ever more important now to prevent the gap from growing between those who are embracing analytics and those who are lagging behind.
---

Curating a healthy data culture is ever more important now to prevent the gap from growing between those who are embracing analytics and those who are lagging behind.
