---
title: What is data visualisation?
date: 2020-04-07T17:04:01.712Z
author: Mia Hatton
description: >-
  Data visualisation makes data insights accessible to everyone and empowers
  your teams to take data-driven actions efficiently.
tags:
  - data analysis
  - insight
  - visualisation
  - business intelligence
categories:
  - glossary
  - offerings
image: /images/uploads/data-visualisation.jpg
executive: >-
  Data visualisation empowers all of the teams in your organisation to make data-driven decisions and act on insights from your data. By displaying data as visual charts such as bar charts, line charts and maps, gaining insight is a quicker and more accessible process. This saves time and resources in communicating and understanding data.  

  Data visualisation helps businesses:  


  * to make data-driven decisions.

  * to reduce the cost and time required to gain insight from data.

  * to make actionable data insights available across the organisation to
  improve KPIs.
departmenthead: >-
  Data visualisation makes data insights accessible to everyone and empowers
  your teams to take data-driven actions efficiently. You might build a shared
  data dashboard for your team so that everybody can gain a clear overview of
  your team's performance quickly, and act on the insight. You may also build
  reports and dashboards to share with executive teams and clients to
  demonstrate the value of your team's work and highlight KPIs.


  You may need this service if:


  * your team needs a clear, at-a-glance overview of performance on a regular
  basis.

  * your team regularly reports on performance to executives or clients.

  * your team is missing opportunities to act on data insights due to lack of
  time or analysis skills.

  * you want to gain insight into customer behaviour, market segmentation and
  marketing campaign performance.


  KPIs you should consider measuring for this are:


  * improved **conversion rate** at all levels of your funnel

  * reduced **stock waste**

  * improved **retention rates**

  * reduced **overspend**
technical: >-
  Data visualisation makes your company's data more accessible to those who can
  act on the insights to improve performance. Implementing data visualisation
  may require you to perform a data audit to get an overview of your data's
  sources, storage locations and relationships (see[data
  modeling](https://nightingalehq.ai/knowledgebase/glossary/what-is-data-modeling)). Introducing data
  visualisation tools in your organisation will reduce the load on IT teams as
  access to data insights can be passed on to team leaders.


  Data visualisation helps deliver:


  * actionable insights and feedback

  * improved accessibility of data insights


  Get this service if you encounter:


  * repeated requests from different teams for data retrieval

  * a lack of understanding of data insights throughout your organisation


  Key criteria to consider are:


  * What tools and tech stacks should you use to visualise your data?

  * How will you maintain the security of your data?

  * Will staff require training to access, use and interpret data visualisation
  dashboards?

  * Do you have the time and resources to set up a data visualisation system?
---
Welcome to the Nightingale HQ overview of data visualisation services. Here we aim to introduce people to what they need to know.

## Definition of data visualisation

From [Wikipedia](https://www.wikipedia.org/wiki/Data_visualisation)

> Data visualization is the graphic representation of data. It involves producing images that communicate relationships among the represented data to viewers of the images. This communication is achieved through the use of a systematic mapping between graphic marks and data values in the creation of the visualization. This mapping establishes how data values will be represented visually, determining how and to what extent a property of a graphic mark, such as size or color, will change to reflect change in the value of a datum.