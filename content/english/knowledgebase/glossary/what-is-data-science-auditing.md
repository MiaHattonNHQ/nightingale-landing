---
title: What is data science auditing?
date: 2020-04-08T18:16:18.553Z
author: Mia Hatton
description: >-
  Data science auditing is a review of your data science solutions to assess their quality, security and compliance.
tags:
  - data science
  - data security
  - strategy
categories:
  - glossary
  - offerings
image: /images/uploads/data-science-audit.jpg
executive: >-
  Data science auditing ensures that your organisation is producing models and solutions that meet your strategic goals and are secure, compliant, and of good quality.  

  Data science auditing helps businesses:  

  - to produce good quality data science solutions.
  
  - to ensure that resources are being spent on data science solutions that meet your strategic goals.

departmenthead: >-
  Data science auditing helps teams ensure that the models you are using are of good quality, compliant, and secure.  

  You may need this service if:  

  - you are using models that were developed in-house but experiencing problems with their accuracy and compliance.

  - you are concerned about the security of the models you use.  
  
  KPIs you should consider measuring for this are:  

  - improved accuracy of predictive models.

  - improved compliance and security of your data science solutions.  


technical: >-
  Data science auditing involves an outside expert reviewing your code, documentation and solution or model to assess reproducibility, quality, and how well it meets your business needs.  

  Data science auditing helps deliver:  

  - review of your solution by an outsider with relevant expertise

  - data verification

  - improvements to your model's reproducibility and quality

  - a second, outsider opinion on your model

  
  Get this service if you encounter:  

  - difficulty assessing the quality of your data science solutions.  

  
  Key criteria to consider are:  

  - Are you able to clearly communicate the goals of your data science project and the problems that it is trying to solve?

  - Do you have the resources to act on the findings of a data science audit?

---

Welcome to the Nightingale HQ overview of data science auditing services. Here we aim to introduce people to what they need to know.  

## Definition of data science auditing

Data science auditing is a review of your data science solutions to assess their quality, security and compliance.  
