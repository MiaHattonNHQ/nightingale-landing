---
title: What is data strategy?
date: 2020-04-09T09:01:18.553Z
author: Mia Hatton
description: >-
  Closely linked to a business strategy, a data strategy is simply the roadmap of how a company will utilise data to optimise their business.
tags:
  - data science
  - strategy
  - insight
  - business intelligence
categories:
  - glossary
  - offerings
image: /images/uploads/data-strategy-2.jpg
executive: >-
  Data strategy enables companies to analyse progress and make better business decisions, but it also helps to create a thriving data environment where it can be easily shared and accessed, safely stored and standardised, and viewed as a central resource to all departments. While it can be beneficial for a company to outsource data science skills, we recommend building up data skills within your team by upskilling and recruiting where possible as part of your data strategy.  

  Data strategy helps businesses:  

  - solve business problems

  - enhance day-to-day operations

  - advance the overall functioning

  - improve decision making

departmenthead: >-
  Data strategy helps teams to work towards a common goal while ensuring a standardised way of working with data.  

  You may need this service if:  


  - your business has poor data management among departments causing data-related issues and incompatibility between projects.

  - your business suffers from data silos due to friction between departments or technological factors, which prevents teams from seeing the bigger picture.


  If you want to measure the performance of your data strategy, you should set KPIs that are in line with your business strategy and associated KPIs. Your data strategy should always be linked to your business priorities and may include a detailed [AI strategy](https://nightingalehq.ai/knowledgebase/glossary/what-is-ai-strategy/) section.

technical: >-
  Data strategy provides a clear, actionable plan to make data more accessible by relevant departments while ensuring that it is stored efficiently and securely. Technical teams should take the lead in setting a standardised way of working with data and work with executive teams to ensure that data is being used to meet business goals.  

  Data strategy helps deliver:  

  - improved data modelling and database structure.

  - [business intelligence](https://nightingalehq.ai/knowledgebase/glossary/what-is-business-intelligence) and AI adoption.

  
  Get this service if you encounter:  

  - a lack of cohesion between departments when it comes to using data.

  - ad hoc or reactive requests for data insights, which can lead to inefficiencies.

  
  Key criteria to consider are:  

  - What tools should the organisation implement to store data securely and efficiently?

  - How should business intelligence be incorporated into the strategy and which tools will be required?

  - Will your database structure need to be changed to meet the data strategy?

  - What are the data needs of all departments and how can the data strategy meet them?

---

Welcome to the Nightingale HQ overview of data strategy services. Here we aim to introduce people to what they need to know.  

## Definition of data strategy

Data strategy refers to a company's vision for how data will be used to help achieve the company's business goals.  
