---
title: What is developing a centre of excellence?
date: 2020-04-08T13:34:44.128Z
author: Mia Hatton
description: >-
  Developing a centre of excellence promotes collaboration in your business and defines best practice for a specific focus area (e.g. AI, data science, analytics or business intelligence) to drive progress towards your strategic goals.
tags:
  - strategy
  - data culture
  - training
categories:
  - glossary
  - offerings
image: /images/uploads/centre-of-excellence.jpg
executive: >-
  Developing a centre of excellence promotes collaboration in your business and defines best practice for your specific focus area - whether it be AI, data science, analytics or business intelligence - to drive progress towards your strategic goals. A centre of excellence provides a central department of expertise that can be leveraged to build capability and enthusiasm across different departments.  
  
  Developing a centre of excellence helps businesses:  

  * foster a positive data culture

  * define best practices

  * promote collaboration

  * provide a way to build skills and solutions that support your [data science strategy](https://nightingalehq.ai/knowledgebase/glossary/what-is-data-science-strategy)  

departmenthead: >-
  Developing a centre of excellence helps teams to build skills, knowledge and progress in the area of focus. If your organisation has a [data strategy](https://nightingalehq.ai/knowledgebase/glossary/what-is-data-strategy), a data science strategy or an AI strategy, developing a centre of excellence is a tried and tested method for ensuring that all teams are aligned towards the strategic goals and are progressing towards them.  

  You may need this service if:  

  * you have a [data science](https://nightingalehq.ai/knowledgebase/glossary/what-is-data-science) strategy but are not seeing enough progress towards the strategic goals.

  * you are having trouble fostering enthusiasm and positive culture for data science and AI.

  * your team lacks the skill and knowledge required to implement data initiatives.  

  
  KPIs you should consider measuring for this are:  


  * improved progress towards strategic goals

  * increased enthusiasm for the focus area

  * increased up-skilling of staff in the focus area


technical: >-
  Developing a centre of excellence sets and maintains best practices for the focus area, creating a central function that can make software and technology decisions, avoiding the risk of shadow IT and improving IT efficiency across the organisation. Developing a centre of excellence can also create leadership opportunities for technologists in the organisation, improving staff retention by allowing career progression.  

  Developing a centre of excellence helps deliver:  

  * a central function for software and technology decision making.

  * increased awareness of IT issues across the organisation.

  * improved skills across the organisation.  
  
  Get this service if you encounter:  

  * shadow IT - software and technology decisions being made by different departments without IT being involved.

  * a lack of appreciation for the focus area by other departments.

  * poor staff retention due to a lack of career progression opportunities.  

  Key criteria to consider are:  

  * Does enough talent exist within the organisation to develop a centre of excellence or will you need to recruit experts?

  * How will you up-skill staff from relevant departments?

  * How will the decisions of the centre of excellence be communicated and upheld across the organisation?  

---

Welcome to the Nightingale HQ overview of developing a centre of excellence services. Here we aim to introduce people to what they need to know.  

## Definition of developing a centre of excellence

From [Wikipedia](https://en.wikipedia.org/wiki/Center_of_excellence)

> A centre of excellence (COE) is a team, a shared facility or an entity that provides leadership, [best practices](https://en.wikipedia.org/wiki/Best_practice), research, support and/or training for a focus area.

The focus area could be data analytics, data science, AI or business intelligence.
