---
title: What is data science?
date: 2020-04-08T15:11:18.553Z
author: Mia Hatton
description: >-
  Data science helps businesses to build better products and make better, data-driven decisions.
tags:
  - data science
  - product development
  - insight
  - strategy
categories:
  - glossary
  - offerings
image: /images/uploads/data-science.jpg
executive: >-
  Data science applies scientific methods to data to reveal insights and predictions that can help you to meet your business goals. There are two key goals in applying data science to your business: building better products and making better decisions. Data science is applied to building intelligent features and automation into products to make them more appealing and profitable to customers. Data analysis and machine learning can be applied to business metrics to drive better recommendations and predictions.  

  Data science helps businesses:  

  - build better products.

  - make better decisions.

departmenthead: >-
  Data science can be applied in all areas of your business. It helps development teams to improve the products they build, helps sales and marketing teams with market segmentation and advertising, and supports financial decision-making.  

  You may need this service if:  

  - you are building a product that could be improved by intelligent features.

  - your team has access to large volumes of data but is not using it to make predictions or decisions.  
  
  KPIs you should consider measuring for this are:  

  - increased sales of your product

  - improved customer retention

  - increased lead generation

  - improved cash flow

  - increased profits  

technical: >-
  Data science can be applied to your product by integrating usage data collection solutions and implementing features that analyse the usage data and apply algorithms to the usage data to drive improved performance and user experience.  

  Data science helps deliver:  

  - improved performance through analysis of usage data.

  - personalised content that can drive increased up-sells and interactions.  

  
  Get this service if you encounter:  

  - missed opportunities to apply usage data to improving performance.

  - lack of personalised content in your product.

  - lack of understanding of how your product can be improved.  

  
  Key criteria to consider are:  

  - Do you have appropriate data storage solutions in place that will keep user data secure?

  - Do you have the time and resources to apply your analysis to make improvements to your product?

  - Does your product have enough users to generate actionable data insights?  

---

Welcome to the Nightingale HQ overview of data science services. Here we aim to introduce people to what they need to know.  

## Definition of data science

From [Wikipedia](https://www.wikipedia.org/wiki/Data_Science)

> Data science is a multi-disciplinary field that uses scientific methods, processes, algorithms and systems to extract knowledge and insights from structured and unstructured data. Data science is related to data mining and big data.
