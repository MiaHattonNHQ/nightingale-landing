---
title: What is Google Cloud?
date: 2020-04-09T14:35:18.038Z
author: Mia Hatton
description: >-
  A cloud computing solution such as Google Cloud provides pay-as-you-go access to vast data storage and computing capabilities and allows you to scale the costs as you grow, avoiding expensive set-up costs.  
tags:
  - google cloud
  - cloud service
  - ai
  - data storage
  - data analysis
categories:
  - glossary
  - tech stacks
image: /images/uploads/google-cloud.png
executive: >- 
  Implementing [data science](https://nightingalehq.ai/knowledgebase/glossary/what-is-data-science) and [AI](https://nightingalehq.ai/knowledgebase/glossary/what-is-ai) in your business requires vast data storage capabilities and expensive infrastructure. A cloud computing solution such as Google Cloud provides pay-as-you-go access to these systems and allows you to scale the costs as you grow, avoiding expensive set-up costs.  

  Google Cloud helps businesses:


  - adopt AI without expensive set-up costs

  - develop high-quality applications that collect and leverage data to maximise revenue
departmenthead: >-
  Google Cloud helps teams adopt AI solutions that can drive growth and improve performance, as well as providing solutions to host applications and build improved functionality that will drive sales. Google Cloud is a cloud computing platform, which allows pay-as-you-go access to storage and computing services without expensive infrastructure set-up fees.  

  Signs your department should invest in this are:  


  - you are developing an application and need a hosting solution that is cheap to implement and will scale as you grow

  - you are planning to adopt AI in your team and need access to data storage and machine learning tools at a price that will scale with your needs

  KPIs you should consider measuring for this are:  


  - improved sales when AI features are implemented

  - savings on infrastructure investment

  - improved efficiency of resource management

  - improved product performance
technical: ''
---
Welcome to the Nightingale HQ overview about Google Cloud services. Here we aim to introduce people to what they need to know.

## Definition of Google Cloud

From [Wikipedia](https://en.wikipedia.org/wiki/Google_Cloud_Platform):

> Google Cloud Platform (GCP), offered by Google, is a suite of cloud computing services that runs on the same infrastructure that Google uses internally for its end-user products, such as Google Search and YouTube. Alongside a set of management tools, it provides a series of modular cloud services including computing, data storage, data analytics and machine learning. Registration requires a credit card or bank account details.

Google Cloud is a [cloud service provider](https://nightingalehq.ai/tags/cloud-service). Other cloud vendors to consider are:

- [Microsoft Azure](https://nightingalehq.ai/knowledgebase/glossary/what-is-azure)
- [Amazon Web services (AWS)](https://nightingalehq.ai/knowledgebase/glossary/what-is-aws)
- [IBM Cloud](https://nightingalehq.ai/knowledgebase/glossary/what-is-ibm-cloud)

## Key technologies

There are more than 90 services available from Google Cloud, across more than 15 categories. These products include the following:

### Data storage and management

- **Cloud Storage** provides highly durable, scalable global storage for object data that can be accessed instantly and integrated into applications. Four different storage classes are available, differentiated by frequency of data access, to allow you to manage costs with minute detail. Transfers between these classes can be automated using triggers.
- **Cloud SQL** is a fully managed relational database to support analytics or operational activities, saving the need to manage the underlying infrastructure or tasks like backups
- **Cloud BigTable** is a petabyte-scale noSQL database service to support large analytical and operational workloads
- **Cloud Firestore** is a scalable noSQL database service that can store, sync and query data to support your mobile, web or IoT app
- There are a suite of tools for data migration and cloud set-up, including **BigQuery Data Transfer Service** and **Migrate for Compute Engine**.

### Analytics

- [**BigQuery**](https://nightingalehq.ai/knowledgebase/glossary/what-is-google-cloud-bigquery) is a serverless cloud data warehouse service that integrates data from diverse services. As well as being highly scalable it has in-built machine learning features that allow you to gain insight and predictions whilst streaming large volumes of data.
- **Data Catalog** is a metadata management service that empowers organizations to quickly discover, manage, and understand all their data in Google Cloud. Powered by Google Search technology, it makes it easy to discover and organise your data.
- **Cloud Dataprep by Trifacta** is an intelligent data preparation service that allows you to visually explore, clean and prepare your data for analysis, reporting, and machine learning..

### AI and machine learning

- **AutoML** is a suite of machine learning tools that make training high-quality models accessible to non-experts. It features a simple graphical user interface that can be used to train, evaluate, improve and deploy machine learning models. The tools include **AutoML Vision** and **AutoML Translation**.
- **Cloud AI Building Blocks** is a set of AI tools that can be seamlessly integrated into your applications to add sight, language, conversation, and structured data.
- **DialogFlow** is a service that allows you to build conversational interfaces - such as chatbots - and put them into production on messaging platforms, IoT devices, websites, and other applications.

## Security and Compliance

Google Cloud services are built on the same secure-by-design infrastructure that is used by Google itself.  Google has a policy of creating trust through transparency so you can read all about their security, privacy and compliace measures by visiting their [Trust and Security](https://cloud.google.com/security/) centre.  

### Security

Google Cloud's global network of data centres feature multi-layered security and encryption at rest and in-transit by default. Security products from Google Cloud include **Cloud Security Command Center** and **Cloud Security Scanner**.  
View the Google Cloud Security Showcase [here](https://cloud.google.com/security/showcase/).  

### Privacy

Google Cloud gives you complete ownership and control over your data, providing tools that allow you to determine where your data will be stored, secure it, and maintain control of who has access to your data. Google Cloud also offers multiple-factor authentication on data and services, and protects sensitive data with advanced tools like phishing-resistant Security Keys.  
Read more about Google Cloud data privacy [here](hhttps://cloud.google.com/security/privacy/).  

### Compliance

Google Cloud products are independently verified for security, privacy and compliance and there are a suite of resources available to help users ensure that their security and privacy settings meet regulations. There are also industry-specific resources available for financial services, government and public sector, and healthcare and life sciences compliance procedures.  
Read more about Google Cloud compliance [here](https://cloud.google.com/security/compliance/).  

## Pricing

With no up-front costs or termination fees, and a competitive, scalable pricing structure, Google Cloud's pricing structure offers many opportunities to save costs of computing infrastructure. These include:

- **Sustained-use discounts** for workloads that run for most of the month
- Discounts for services that can be interrupted (such as data mining)

The [Google Cloud Platform Pricing Calculator](https://cloud.google.com/products/calculator/) helps you to budget and estimate costs of using Google Cloud services.

Creating an [account](https://cloud.google.com/free/) on Google Cloud provides:

- $300 free credit to spend on Google Cloud Platform in your first 12 months.
- acccess to the Always Free programme that confers limited access to many common Google Cloud resources - such as Cloud Storage and Vision AI - for free.

Beyond the free services, Google Cloud offers the choice between a pay-as-you-go (PAYG) model and comitted use contracts (or a combination of the two). With either option, you are billed monthly for the services you use or reserve.  

### Pay as you go (PAYG)

A great option for scalability, with PAYG pricing you pay only for the Google Cloud services you use. This allows you to grow and innovate without being hit by high set-up and scaling upfront costs.

Each service is priced based on a number of *components*. For example, Cloud Storage pricing is based on storage space, network usage (when data is read from the service) and number of operations (events where data is added or changed).

### Committed use contracts

If you are confident of the computing power and storage space you will need for your application in advance, you can save on costs by purchasing committed use contracts for your estimated upcoming usage. The discount is up to 57% for most resources like machine types or GPUs. The discount is up to 70% for memory-optimized machine types. There are no upfront payments for committed use contracts and you are still billed monthly as with PAYG.

### Purchase Google Cloud services through an Google Cloud partner

As well as having the option to buy directly from Google Cloud, you can go through a Google Cloud Partner. Google Cloud partners can help you to migrate to, build on, or work in the cloud. Many Google Cloud partners are value-added resellers who can save costs by ensuring you only buy what you need, and by helping you to get set up efficiently. Other partners are managed service providers who can take care of the administration side of your cloud computing and allow you to focus on growth and innovation.
You can find an APN partner [here](https://cloud.withgoogle.com/partners/).

### Support

All Google Cloud Platform customers have access to basic support for free, providing customer service via phone and chat for billing issues. There is comprehensive documentation available for all services, but for technical support on top you can upgrade to a Development, Production, or Business-critical support plan, from $100 per month.
Learn more about the support plans [here](https://cloud.google.com/support/).  
