---
title: What is business intelligence?
date: 2020-04-07T18:34:15.885Z
author: Mia Hatton
description: >-
  Business intelligence refers to the systematic collection, integration, analysis and reporting of data from all areas of your business to drive better decision making.
tags:
  - data sources
  - data analysis
  - business intelligence
  - insight
  - strategy
  - data science
categories:
  - glossary
  - offerings
image: /images/uploads/business-intelligence.jpg
executive: >-
  Business intelligence gives you a clear view of your company's performance and progress towards your strategic goals. This insight comes from applying analysis and modeling to data from all areas of your business so that you are fully informed at all times, and makes actionable insights available to all teams so that improvements to processes are made efficiently.  

  Business intelligence helps businesses:  


  * streamline processes by making actionable insights available to people at all levels of the organisation.

  * gain a clearer view of the whole organisation's performance.

  * develop a clear strategy for growth based on insights.  
departmenthead: >-
  Business intelligence helps teams to gain valuable insight into their performance and act on that insight efficiently.  

  Business intelligence brings value to different departments throughout an organisation:  


  * **Sales teams** use business intelligence to gain a clearer view of conversion rates and insights into the quality of leads passed to them, helping them to focus their attention, keep track of their targets and improve their performance.

  * For companies offering physical products, having insight into seasonal trends helps **purchasing departments** to ensure that inventory is adequate for periods of high demand. 

  * Getting an overview of churn rates, reviews and ticketing supports training, and improves the quality and personalisation of **customer service**, leading to improved customer retention and advocacy.

  * **Finance departments** can stay ahead of potential problems such as overspending and revenue downturns thanks to the predictive properties of business intelligence.


  KPIs you should consider measuring for this are:


  * Improved conversion rate at all levels of your funnel.

  * Reduced stock waste.

  * Improved retention rates.

  * Reduced overspend. 
technical: >-
  Business intelligence solutions make actionable insights available to all relevant stakeholders, reducing their dependency on IT teams to make insights available. Business intelligence solutions can be built into products to deliver insights both internally and directly to clients. For business intelligence to be successfully implemented, the structure of available data must be understood and it should be stored efficiently to allow secure and efficient access from different stakeholders.  

  Business intelligence helps deliver:


  * actionable insights and feedback.

  * tiered availability of data and results of analyses to different stakeholders.

  * insights that are tailored to the user.


  Get this service if you encounter:


  * high demand from different departments to understand the data your company holds.

  * a lack of understanding of data analysis throughout your organisation.

  * difficulties aggregating data from disparate sources.

  * a need to offer tiered security access to data and insights.


  Key criteria to consider are:


  * Which tech stack should you use to implement business intelligence?

  * How will you maintain the security of your data?

  * Is your data structured and stored in a way that allows access by your business intelligence solution?

---

Welcome to the Nightingale HQ overview of business intelligence services. Here we aim to introduce people to what they need to know.

## Definition of business intelligence

From [our blog](https://blog.nightingalehq.ai/why-do-you-need-business-intelligence)

> Business intelligence refers to the systematic collection, integration, analysis and reporting of data from all areas of your business - from finance and sales to worker productivity - to drive better decision making.

[*Image source*](https://commons.wikimedia.org/wiki/File:Infruid%27s_Self-Service_BI_Tool_Dashboard.jpg)
