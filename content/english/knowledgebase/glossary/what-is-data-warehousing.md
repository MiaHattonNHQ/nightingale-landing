---
title: What is data warehousing?
date: 2020-04-07T18:34:15.885Z
author: Mia Hatton
description: >-
  Data warehousing helps all teams within a business to make better informed decisions based on accurate insights.
tags:
  - data sources
  - data storage
  - data analysis
  - business intelligence
categories:
  - glossary
  - offerings
image: /images/uploads/data-warehouse.jpg
executive: >-
  Data warehousing refers to building a system that can store large volumes of data from several sources, as a central repository from which all other systems can access the data efficiently. This allows your business to implement a range of data science products, including business intelligence and artificial intelligence.  


  Data warehousing helps businesses:


  * reliably and accurately store large volumes of valuable data.

  * modernise their data usage.

  * create a central repository of all available data to inform better decision making.  

departmenthead: >-
  Data warehousing helps all teams within a business to make better-informed decisions based on accurate insights. If you wish to perform data analysis and data science to make better decisions within your team or to implement intelligent features in your products, you will need data that is stored centrally and efficiently.  


  You may need this service if:


  * data insights come from disparate sources so that gaining insight is time-consuming.

  * you are developing [business intelligence](https://nightingalehq.ai/knowledgebase/glossary/what-is-business-intelligence) for your organisation and need a central repository for historical and current data.


  KPIs you should consider measuring for this are:


  * Reduced time spent analysing data from disparate sources.

  * Improved conversion rates from actionable insights.  
technical: >-
  Data warehousing helps deliver:


  * a single repository of integrated data from one or more sources.

  * a single source of current and historical data.

  * storage of the data for reports / analytics.


  Get this service if you encounter:


  * difficulties aggregating data across your organisation.


  Key criteria to consider are:


  * Time and cost to develop and maintain this resource.

  * Accuracy of data.  

---

Welcome to the Nightingale HQ overview of data warehousing services. Here we aim to introduce people to what they need to know.

## Definition of data warehousing

From [Wikipedia](https://www.wikipedia.org/wiki/Data_Warehousing)

> In computing, a data warehouse (DW or DWH), also known as an enterprise data warehouse (EDW), is a system used for reporting and data analysis, and is considered a core component of business intelligence. DWs are central repositories of integrated data from one or more disparate sources. They store current and historical data in one single place that are used for creating analytical reports for workers throughout the enterprise.

[*Image source*](https://github.com/InnoArchiTech/datascience-ai-machinelearning-resources/blob/master/Big%20Data%20Architecture%20-%20Reference.md)
