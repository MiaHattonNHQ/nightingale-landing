---
title: How can I edit my company?
date: 2020-04-14T09:15:18.553Z
author: Mia Hatton
description: By signing in to the Nightingale HQ app and navigating to your company page.
tags:
  - nightingale hq app
  - partner
  - help for organisations
  - help for partners
categories:
  - app help
image: /images/uploads/edit-profile.png
executive: ''
departmenthead: ''
technical: ''
---
Once signed in, navigate to YOUR COMPANY and click EDIT in the corner of each section to make changes.  

![screenshot of Nightingale HQ AI Connect app showing 'YOUR COMPANY' page](/images/uploads/edit-profile-inline.png)

In order to get accurate partner recommendations on the PARTNERS page, it is most important to complete the YOUR [TAGS](https://nightingalehq.ai/knowledgebase/glossary/what-are-tags/) section, as these are the parameters that will be used to generate your matches.  

For more information on types of tags, take a look at the following help articles:

- [What is a 'Vertical'?](https://nightingalehq.ai/knowledgebase/glossary/what-is-a-vertical/)
- [What is a 'Tech Stack'?](https://nightingalehq.ai/knowledgebase/glossary/what-is-a-tech-stack/)
