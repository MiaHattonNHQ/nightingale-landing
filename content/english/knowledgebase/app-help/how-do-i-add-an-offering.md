---
title: How do I add an Offering?
date: 2020-04-14T07:28:18.553Z
author: Mia Hatton
description: >-
  Your offerings is where you pick what you have to offer to prospective clients.
tags:
  - nightingale hq app
  - partner
  - tags
categories:
  - app help
image: /images/uploads/offerings.jpg
executive: ''
departmenthead: ''
technical: ''
---
If your offering isn't available but is data & AI-related, then use the 'tags' section of your profile to add it. We regularly review the custom tags and promote tags there to the relevant section. The tags are also included in search immediately so you won't miss out on any traffic.  

Read more about the offerings that are already available [here](https://nightingalehq.ai/categories/offerings/).
