---
title: Are there any fees?
date: 2020-04-14T12:33:18.553Z
author: Mia Hatton
description: Nightingale HQ is currently free to use.
tags:
  - nightingale hq app
  - help for organisations
  - help for partners
categories:
  - app help
image: /images/uploads/sign-up-free.png
executive: ''
departmenthead: ''
technical: ''
---
As an organisation or business leader looking for a data or AI consultant, it is currently free to sign up for Nightingale HQ AI Connect and find your match. Once you have matched with a consultant, it is down to you to negotiate your terms outside of the app.  

As a partner offering your consulting services to potential customers, it is also free to register your business on Nightingale HQ AI Connect.
