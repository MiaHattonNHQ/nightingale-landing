---
title: What are Tags?
date: 2020-04-14T08:37:18.553Z
author: Mia Hatton
description: >-
  On my profile I can add 'tags'. What do these do?
tags:
  - nightingale hq app
  - partner
  - tags
  - help for partners
categories:
  - app help
image: /images/uploads/tag.jpg
executive: ''
departmenthead: ''
technical: ''
---
Tags are how we build up a profile of organisations and partners to support the process of matching them up.  

We have tags in different sections but we also have a custom Tag section. In this area, you can add anything that isn't available or doesn't quite fit into profile tag sections. We regularly review the custom tags and promote tags there to the relevant section. Tags are also included in search immediately so you won't miss out on any matches.
