---
title: How can I search for partners?
date: 2020-04-14T09:52:18.553Z
author: Mia Hatton
description: The best way to find partners with your desired skillset is to fill in your own profile.
tags:
  - nightingale hq app
  - partner
  - help for organisations
categories:
  - app help
image: /images/uploads/partner.png
executive: ''
departmenthead: ''
technical: ''
---
## Completing your profile

The more complete your profile is, the better the matches in the PARTNERS section will be. Once signed in, navigate to YOUR COMPANY and click EDIT in the corner of each section to make changes.  

![screenshot of Nightingale HQ AI Connect app showing 'YOUR COMPANY' page](/images/uploads/edit-profile-inline.png)

The most important section to complete is YOUR [TAGS](https://nightingalehq.ai/knowledgebase/glossary/what-are-tags/), as these are the parameters that will be used to find you the best partner matches. For more information on types of tags, take a look at the following help articles:  

- [What is a 'Vertical'?](https://nightingalehq.ai/knowledgebase/glossary/what-is-a-vertical/)
- [What is a 'Tech Stack'?](https://nightingalehq.ai/knowledgebase/glossary/what-is-a-tech-stack/)

## Finding partners

With your updated profile, navigate to the PARTNERS section. Here you will see an ordered list of partners that most closely match your criteria. Here, you can also edit your search options on the left-hand side of the page to further filter results by adding additional tags to your search criteria.  

![screenshot of Nightingale HQ AI Connect app showing 'Partner' page](/images/uploads/partner-match.jpg)
