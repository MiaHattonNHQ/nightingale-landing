---
title: Is my information public?
date: 2020-04-14T10:02:18.553Z
author: Mia Hatton
description:  >-
  For the most part, no. Your profile information is only made visible to partners you start a conversation with. Partner profiles are only visible to users who are signed in to the app.
tags:
  - nightingale hq app
  - help for organisations
  - help for partners
categories:
  - app help
image: /images/uploads/data-platform-security.jpg
executive: ''
departmenthead: ''
technical: ''
---
If you are a customer looking for a data & AI partner who can help you, partners whom you click through to or message will be able to see your details for the purpose of engaging with you. No other partners will be able to see your data. Your data is not available to other customers. Your data is not distributed to others.  

If you are a partner, your partner profile is made available to customers so that they can potentially engage with you. Customers must be signed up to see this information and specific information about you as an individual is kept private.
