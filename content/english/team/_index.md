---
title: "Team"
menu:
  main:
    weight: 2
    parent: company
---

Nightingale HQ is a diverse group of individuals who share the same goals - to make AI accessible and help business adopt it successfully. 
