---
title: Shaun Faulkner
weight: 4
image: >-
  images/team/Shaun Faulkner - Junior Full Stack Web Developer at Nightingale
  HQ.png
role: Software Engineer
social:
  - icon: ti-linkedin
    link: 'https://www.linkedin.com/in/shaunfaulkner/'
  - icon: ti-twitter-alt
    link: 'https://twitter.com/faulkner368'
  - icon: ti-github
    link: '#'
---

Shaun is a Software Engineer at Nightingale. He started his career as a data analyst and worked in logistics and security analytics.  His interest in web development led him to setup his own IT consultancy where he provided remote/onsite support to clients. He is in the final year of a BSc degree in Computer Science with Open University and his wheelhouse is Python, which he has been using for years.Outside of work he enjoys the odd endurance event and is a first responder volunteer.