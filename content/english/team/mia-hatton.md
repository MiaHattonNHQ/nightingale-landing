---
title: Mia Hatton
weight: 5
image: images/team/Mia Hatton - Data Science Apprentice at Nightingale HQ.png
role: Data Science Apprentice
social:
  - icon: ti-linkedin
    link: 'https://www.linkedin.com/in/miablundell/'
  - icon: ti-twitter-alt
    link: 'https://twitter.com/MiaBlunders'
  - icon: ti-github
    link: '#'
---

Mia is an experienced science communicator and trainer who is now merging these talents to become a Data Scientist. She is currently studying for her Data Science degree at Cardiff Metropolitan University and is putting it also into practice as an apprentice in Nightingale HQ.
Alongside a science degree from the Open University, she has a number of years experience in presenting, science content development and training consultancy across several countries including the Czech Republic, Norway, Malaysia and her home of Wales.

In 2015 she co-founded Letterbox Lab, offering a series of innovative science kits designed to help families to play with science. Letterbox Lab was named 'Retail Start-up of the Year' at the Wales Start-up Awards 2019. Outside of work, Mia enjoys playing Dungeons and Dragons and caring for her curious bearded dragon, Bert. 