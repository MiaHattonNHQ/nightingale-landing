---
title: Sarah Williams
weight: 2
image: images/team/sarahwilliams.jpg
role: CTO
social:
  - icon: ti-linkedin
    link: 'https://www.linkedin.com/in/sarah-williams-cardiff/'
  - icon: ti-twitter-alt
    link: 'https://twitter.com/ff0brickcode'
  - icon: ti-github
    link: 'https://github.com/ff0brickcode'
---

Sarah is an experienced software architect, engineer and founder who is well versed in leading software teams in startups and scaling companies. She has a strong track record of major project releases across fintech, medtech and multinationals and consults on everything from system and software architecture to team culture. 

She runs her own development consultancy specialising in web apps and is on the founding team at SorbetHQ, a property maintenance software and Nightingale HQ. She was recently honoured as Wales 35 Under 35 and is a finalist in Booking.com 2020 Technology Playmaker Awards..