---
title: Ruth Kearney
weight: 3
image: images/team/ruthkearney.jpg
email: ruth@nightingalehq.ai
role: Product & Commercial Director
social:
  - icon: ti-facebook
    link: '#'
  - icon: ti-twitter-alt
    link: 'https://twitter.com/ruthtagged'
  - icon: ti-github
    link: '#'
---

Ruth is 15 years working in the education sector, developing practical and transformative experiences at all levels. She is an award-winning programme developer who is passionate about the value that an action learning approach to education brings to both student and industry. 

She recently established the Innovation School for the largest European co-working company Talent Garden. Here, she led a team to create a range of courses to accelerate the most in-demand digital skills for industry including executive masterclasses and data science bootcamps. 

Ruth loves firsts and seeing good ideas happen. She setup the first VC in Residence programme in Europe, developed Ireland's first accredited Returners Programme in Trinity College Dublin and proudly contributed to the first run of Fast.ai is the most internationally recognised data-science course for coders originated from The Data Institute at the University of San Francisco (USF). Prior to TAG, she worked in Trinity for 7 years extensively rolling out their innovation & entrepreneurship programmes. She was also heavily involved in business development and led on several Horizon 2020 and EIT Climate/Health and Raw Material grants during her time there. Ruth worked in the Technology Transfer & Incubation Services of Technological University Dublin (TU Dublin) helping faculty commercialise their research.
