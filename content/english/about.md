---
title: "About"
seo:
  title: "About"
  description: ""
date: 2019-04-09T17:01:46Z
menu:
  main:
    weight: 2
    parent: company
---


Nightingale HQ is a diverse group of individuals who share the same goal - to make Your Business and Your People. AI-ready. This emerging tech will be integrated into every business in the next few years and we are developing a platform that combines a robust set of data, AI and business expertise. Our track record lies in helping individuals and organisations generate value, both cost saving and revenue generating no matter what stage of the maturity curve they are at. Our platform consists of three core products;  

- AI Learn offers online and on-demand courses and mentoring programmes to help build core competencies
- AI Connect uses our AI algorithm to match you to the right expertise and skills to help deliver AI projects
- AI Direct is our leadership toolkit that supports managers on the road to change with AI. We offer a set of practical tools and workshops for individual managers and teams to accelerate their strategic efforts. 

## Who was Florence Nightingale?

Almost everyone has heard of the Lady with the Lamp. Nightingale worked as a nurse during the Crimea War and her local intervention made the difference in thousands of lives. But did you know she was one of the world's first and most successful data scientists?

Florence performed comprehensive studies into sanitation and medical care that revolutionised the way patients were cared for. Instead of publishing in a journal for medicial practitioners or statisticians to read, she made her work consumable by government and military people who could use it immediately.

Turning complex, messy, real-world experience into actionable, simple insight made a huge impact and it's this process we're seeking to achieve too. Thus we put our aspiration first and foremost in our name as a constant reminder.

## Our Story
Nightingale HQ emerged when Steph Locke, CEO, realised that in order to help more people in her original business, Locke Data, she would need to scale, and rather than scale her consultancy, she would achieve this instead by creating a platform.

The first step was to create the consultancy marketplace where both organisations and consultancies can register. Applying a matching algorithm, organisations that are looking for help with a project will be shown their top matches, based on the details they have entered such as the tech stacks they work with, their industry, etc. Partners with best matches to these requirements appear first on the list.

With this functionality set up, we are now looking at developing the training and strategy sections of the platform.

## The Founders
Steph is a Microsoft Most Valued Professional in both Data Platforms and Artificial Intelligence, a combination awarded by Microsoft to only two other people in the world. She regularly presents and keynotes globally on the topics of AI, Data Science, and the integration of these capabilities into business.

Sarah is a dedicated senior technologist with refined mentoring and presenting skills. She has been recognised for her work in startups with awards like Wales 35 under 35.

Ruth is an exceptional educational programme director, with a track record for innovative industry-led programmes in Trinity College Dublin and Talent Garden. Most recently, she also helped with the first Irish run of fast.ai originally developed by University of San Francisco.

Find out more about the team in our dedicated [Team page](./team).
