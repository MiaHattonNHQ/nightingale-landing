---
title: "Careers"
seo:
  title: "Careers"
  description: ""
date: 2019-07-01T17:01:46Z
draft: false
menu:
  resources:
    weight: 2
---

# Work with Nightingale HQ
Nightingale HQ is an up and coming AI-focused startup in Cardiff, Wales and Dublin, Ireland. 
We're dedicated to growing our team and the local tech community by hiring sustainably and always having opportunities for people to get into tech and AI. If you are interested in what we do, get in touch email hello@nightingalehq.ai