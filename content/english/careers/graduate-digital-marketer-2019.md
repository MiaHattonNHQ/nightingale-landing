---
title: "[CLOSED] Graduate Digital Marketer"
seo:
  title: "Graduate Digital Marketer"
  description: ""
date: 2019-07-01T17:01:46Z
expirydate: 2019-08-01T17:01:46Z
draft: false
layout: "wrapped"
weight: 1
extract: Build up digital marketing skills and experience with this 6 month placement.
---


The Graduate Digital Marketer will work at Nightingale HQ for an initial 6 months, during which time they will assist Nightingale HQ with setting up a robust marketing strategy and highly automated digital marketing tactics. Whilst working at Nightingale HQ, they will also work towards their ILM qualification.

We expect the Digital Marketer to start in September.

This position is made available under the Cardiff Capital Region Graduate Scheme. We are committed to paying all interns at the National Living Wage. The qualification will be funded by Welsh Government, so no loans or additional expense will be required to get the ILM certification.

Under the scheme, any graduate may apply for the role whether a recent graduate or not. People from any age or employment history with an interest in becoming a digital marketer are welcome to apply. 

## Takeaways from the role
- A strong foundation in scalable marketing techniques and automation
- A network of tech startups and other coworking businesses
- Building a startup, via real experience and training
- An open, non-confidential portfolio of results
- An ILM qualification

## Duties & responsibilities
- Creating content for our marketing sites and social media
- Creating an SEO strategy and developing content that aligns with it
- Set up paid advertising along with retargeting across multiple channels
- Develop automated drip marketing campaigns
- Create sales assets and playbooks
- Inform and contribute to the companies positioning and marketing strategy
- Develop effective press releases and help us build relationships with local journalists
- Additional marketing related tasks identified by the intern as being required

## Criteria
### Scheme criteria
- A degree

### Essential criteria
- Strong written English skills
- Some previous experience of employment
- Demonstrable interest in technology and digital automation

### Desired criteria
You’ll stand out if you have any or all of these:

- A marketing-specific degree
- Interested in Artificial Intelligence and technology
- Previous experience with Hubspot or Google marketing products
- Coding experience, especially with HTML and CSS
- Experience in video creation or photography

## The details
- 35 hours a week
- £16,380 per annum
- Based in central Cardiff or Caerphilly. Some remote working is available
- Minimum placement length of 6 months
- Flexible working around core hours of 10 – 3
- Reporting to Steph Locke CEO

## The application process
This role is available through the Cardiff Capital Region Graduate Scheme. Please apply via the [CCR site](https://www.ccrgraduatescheme.wales/graduates/graduates-jobs/graduate-digital-marketer/).


### The process
CCR will be shortlisting applicants and then we'll do an online call as a first interview. This first interview will go through the role's criteria and provide a baseline assessment. The next stage will be a face to face interview with the founders to understand what unique perspective you would bring to the role.